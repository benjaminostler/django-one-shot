from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoItem, TodoList


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {"todos": todos}
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    todo_items = TodoList.objects.get(id=id)
    context = {
        "todo_items": todo_items,
    }
    return render(request, "todos/detail.html", context)
